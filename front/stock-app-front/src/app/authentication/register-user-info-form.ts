import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {passwordValidator, underAgeValidator} from './validators';
import {FormWrapper} from '../shared/form-wrapper';

export interface RegisterUserInfo {
  email: string;
  password: string;
  birthData: Date;
}

export class RegisterUserInfoForm implements FormWrapper<RegisterUserInfo> {
  readonly controls = {
    email: this.fb.control('', [Validators.email, Validators.required]),
    password: this.fb.control('', [Validators.required, passwordValidator()]),
    birthData: this.fb.control('', [Validators.required, underAgeValidator()]),
  };
  readonly form = this.fb.group(this.controls);

  get(controlName: keyof RegisterUserInfo): AbstractControl {
    return this.form.get(controlName);
  }

  load(value: Partial<RegisterUserInfo>): void {
    this.form.reset(value);
  }

  constructor(private readonly fb: FormBuilder) {
  }
}

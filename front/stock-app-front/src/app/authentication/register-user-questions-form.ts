import {FormWrapper} from '../shared/form-wrapper';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuestionBase} from './register-model';

export class RegisterUserQuestionsForm implements FormWrapper<any> {
  readonly controls: any = {};
  form: FormGroup;

  questionsList: QuestionBase<string>[];

  get(controlName: string): AbstractControl {
    return this.form.get(controlName);
  }

  load(value: Partial<any>): void {
    this.form.reset(value);
  }

  constructor(private readonly fb: FormBuilder,
              private readonly questions: QuestionBase<string>[]) {
    this.initQuestionForm(questions, fb);
  }

  initQuestionForm(questions: QuestionBase<string>[], fb: FormBuilder): void {
    questions.forEach(question => {
      this.controls[question.key] = question.required ?
        fb.control(question.value || '', Validators.required) :
        fb.control(question.value || '');
    });
    this.form = fb.group(this.controls);
    this.questionsList = questions;
  }

}

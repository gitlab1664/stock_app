import {QuestionBase} from '../register-model';

export class RegisterCommand {
  login: string;
  password: string;
  birthData: Date;
  conditionsAgreement: boolean;
  questionsList: QuestionBase<string>[];

  constructor(init?: Partial<RegisterCommand>) {
    Object.assign(this, init);
  }
}

export interface RegisterResponse {
  result: boolean;
  errors: string[];
}

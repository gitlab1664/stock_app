export class LoginCommand {
  login: string;
  password: string;
}

export class LoginResponse {
  result: boolean;
  errors: string[];
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginCommand, LoginResponse} from '../login-model';

@Injectable({
  providedIn: 'root'
})
export class LoginUserService {

  private readonly baseUrl = '/api/login';

  constructor(private http: HttpClient) {
  }

  public login(command: LoginCommand): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(this.baseUrl, command);
  }
}

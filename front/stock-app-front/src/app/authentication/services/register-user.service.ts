import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RegisterCommand, RegisterResponse} from '../dto/authentication-dtos';
import {Observable} from 'rxjs';
import {QuestionBase} from '../register-model';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {

  private readonly baseUrl = '/api/register';

  constructor(private http: HttpClient) {
  }

  public registerUser(command: RegisterCommand): Observable<RegisterResponse> {
    return this.http.post<RegisterResponse>(this.baseUrl, command);
  }

  public getQuestions(): Observable<QuestionBase<string>[]> {
    return this.http.get<QuestionBase<string>[]>(this.baseUrl + '/questions');
  }
}

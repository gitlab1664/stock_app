import {FormWrapper} from '../shared/form-wrapper';
import {AbstractControl, FormBuilder} from '@angular/forms';

export interface RegisterUserAgreements {
  conditionsAgreement: boolean;
}

export class RegisterUserAgreementsForm implements FormWrapper<RegisterUserAgreements> {
  readonly controls = {
    conditionsAgreement: this.fb.control(false)
  };
  readonly form = this.fb.group(this.controls);

  get(controlName: keyof RegisterUserAgreements): AbstractControl {
    return this.form.get(controlName);
  }

  load(value: Partial<RegisterUserAgreements>): void {
    this.form.reset(value);
  }

  constructor(private readonly fb: FormBuilder) {
  }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {RegisterUserInfoForm} from '../register-user-info-form';
import {RegisterUserAgreementsForm} from '../register-user-agreements-form';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {Subscription} from 'rxjs';
import {RegisterUserQuestionsForm} from '../register-user-questions-form';
import {RegisterUserService} from '../services/register-user.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class UserRegisterComponent implements OnInit, OnDestroy {

  userInfoForm: RegisterUserInfoForm;
  userAgreementsForm: RegisterUserAgreementsForm;
  userQuestionsForm: RegisterUserQuestionsForm;

  timeLeft = 20;
  subscription: Subscription;

  constructor(private fb: FormBuilder,
              private service: RegisterUserService) {
  }

  ngOnInit(): void {
    this.userInfoForm = new RegisterUserInfoForm(this.fb);
    this.userAgreementsForm = new RegisterUserAgreementsForm(this.fb);
    this.service.getQuestions().subscribe(value => {
      this.userQuestionsForm = new RegisterUserQuestionsForm(this.fb, value);
    });
  }

  registerUser(): void {
    const questionsList = Object.entries(this.userQuestionsForm.form.value).map(([k, v]) => ({[k]: v}));
    const command = {
      ...this.userInfoForm.form.value,
      ...this.userAgreementsForm.form.value,
      questions: questionsList
    };

    this.service.registerUser(command).subscribe(value => {
      console.log(value);
      alert('register success');
    }, error => {
      console.log(error);
      alert('register failed');
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

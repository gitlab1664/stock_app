import {FormWrapper} from '../shared/form-wrapper';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {passwordValidator} from './validators';

export interface RegisterData {
  email: string;
  password: string;
  birthData: Date;
  agreement: boolean;
}

export class RegisterForm implements FormWrapper<RegisterData> {
  readonly controls = {
    email: this.fb.control('', [Validators.email, Validators.required]),
    password: this.fb.control('', [Validators.required, passwordValidator()]),
    birthData: this.fb.control('', [Validators.required]),
    agreement: this.fb.control(false)
  };
  readonly form = this.fb.group(this.controls);

  get(controlName: keyof RegisterData): AbstractControl {
    return this.form.get(controlName);
  }

  load(value: Partial<RegisterData>): void {
    this.form.reset(value);
  }

  constructor(private readonly fb: FormBuilder) {
  }
}

import {FormWrapper} from '../shared/form-wrapper';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';


export interface LoginData {
  email: string;
  password: string;
}

export class LoginForm implements FormWrapper<LoginData> {
  readonly controls = {
    email: this.fb.control('', [Validators.email, Validators.required]),
    password: this.fb.control('', [Validators.required])
  };
  readonly form: FormGroup = this.fb.group(this.controls);

  constructor(private fb: FormBuilder) {
  }

  get(controlName: keyof LoginData): AbstractControl {
    return this.form.get(controlName);
  }

  load(value: Partial<LoginData>): void {
    this.form.reset(value);
  }
}

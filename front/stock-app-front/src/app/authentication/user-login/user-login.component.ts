import {Component, OnInit} from '@angular/core';
import {LoginForm} from '../login-form';
import {FormBuilder} from '@angular/forms';
import {LoginUserService} from '../services/login-user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

  userLoginForm: LoginForm;

  constructor(private fb: FormBuilder,
              private service: LoginUserService,
              private router: Router) {
    this.userLoginForm = new LoginForm(this.fb);
  }

  ngOnInit(): void {
  }

  loginUser(): void {
    const command = {
      ...this.userLoginForm.form.value
    };
    console.log(command);
    this.service.login(command).subscribe(value => {
      if (value.result) {
        this.router.navigate(['menu']);
      } else {
        if (value.errors) {
          value.errors.forEach(err => console.error(err));
        }
      }
    }, error => console.log(error));
  }

}

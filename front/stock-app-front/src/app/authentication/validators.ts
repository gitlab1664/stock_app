import {AbstractControl, ValidatorFn} from '@angular/forms';

// Password should be at least 1 uppercase and number
const passwordPattern = new RegExp(/^(?=.*[A-Z])(?=.*\d).*$/);

export function passwordValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return !passwordPattern.test(control.value) ? {password: {value: control.value}} : null;
  };
}

export function underAgeValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const today = new Date();
    const birth = new Date(control.value);
    let age = today.getFullYear() - birth.getFullYear();
    const m = today.getMonth() - birth.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birth.getDate())) {
      age--;
    }
    return age < 18 ? {underAge: {value: control.value}} : null;
  };
}

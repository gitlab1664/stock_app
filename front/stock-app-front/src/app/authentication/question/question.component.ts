import {Component, Input, OnInit} from '@angular/core';
import {QuestionBase} from '../register-model';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() question: QuestionBase<string>;
  @Input() form: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

  get isValid(): boolean {
    return this.form.controls[this.question.key].valid;
  }

}

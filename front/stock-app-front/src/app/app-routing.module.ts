import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainScreenComponent} from './authentication/main-screen/main-screen.component';
import {UserRegisterComponent} from './authentication/user-register/user-register.component';
import {UserLoginComponent} from './authentication/user-login/user-login.component';
import {MenuComponent} from './menu/menu/menu.component';

const routes: Routes = [
  {
    path: '',
    component: MainScreenComponent
  },
  {
    path: 'register',
    component: UserRegisterComponent
  },
  {
    path: 'login',
    component: UserLoginComponent
  },
  {
    path: 'menu',
    component: MenuComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {AbstractControl, FormGroup} from '@angular/forms';

export interface FormWrapper<T> {
  readonly controls: Record<keyof T, AbstractControl>;
  readonly form: FormGroup;

  get(controlName: keyof T): AbstractControl;

  load(value: Partial<T>): void;
}

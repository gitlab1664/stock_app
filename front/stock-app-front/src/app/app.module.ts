import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StockMaterialModule} from './material/stock-material/stock-material.module';
import {UserRegisterComponent} from './authentication/user-register/user-register.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MainScreenComponent} from './authentication/main-screen/main-screen.component';
import {QuestionComponent} from './authentication/question/question.component';
import {HttpClientModule} from '@angular/common/http';
import { UserLoginComponent } from './authentication/user-login/user-login.component';
import { MenuComponent } from './menu/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    UserRegisterComponent,
    MainScreenComponent,
    QuestionComponent,
    UserLoginComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StockMaterialModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

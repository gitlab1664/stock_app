const PROXY_CONFIG = [
  {
    context: [
      "/api/register"
    ],
    target: "http://localhost:8000",
    secure: false,
    logLevel: 'debug',
    changeOrigin: true
  },
  {
    context: [
      "/api/login"
    ],
    target: "http://localhost:8000",
    secure: false,
    logLevel: 'debug',
    changeOrigin: true
  }
]

module.exports = PROXY_CONFIG;

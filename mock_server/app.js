const express = require('express')
require('./shared/mongo-connection')
const app = express()
const port = 8000

const echo = require('./echo-router');
const register = require('./register/register-module')
const logging = require('./logging/logging-module')
const stockTimeSeries = require('./stock/time_series_data/stock-time-series-module')
const chalk = require("chalk");
const stockFundamentalData = require('./stock/fundamental_data/stock-fundamental-data-module')

app.use(express.json())

app.use((req, res, next) => {
  console.log(`handling: ${req.originalUrl}`)
  next()
})

app.use(echo)
app.use(register)
app.use(logging)
app.use(stockTimeSeries)
app.use(stockFundamentalData)

app.listen(port, () => {
  console.log(module)
  console.log(chalk.green(`Mock server listening on port ${port}!`))
});


//todo Joi - for request validations
//todo use Chalk to color logs

const mongoose = require('mongoose')
const configs = require('./configs')
const conf = configs.config();

//disconnection is automatic
mongoose.connect(`${conf.mongoUrl}`, {useNewUrlParser: true, useUnifiedTopology: true})
  .then(value => {
    console.log('mongo connection successful')
  })
  .catch(reason => {
    console.log(`mongo connection failed ${reason}`)
  })

mongoose.connection.on('error', err => {
  console.log(err)
})

mongoose.connection.on('disconnecting', () => {
  console.log('mongo disconnecting from mongo')
})

mongoose.connection.on('disconnected', () => {
  console.log('mongo disconnected from mongo')
})

// mongoose.set('bufferCommands', false) // to see if exceptions appears when using model but connection fails, if set manually create schema before use

const env = require("./env.json");
const chalk = require("chalk");

module.exports.config = () => {
  const settings = process.env.NODE_ENV || 'deployment';
  console.log(chalk.blue('-----------------------------------'));
  console.log(chalk.blue(JSON.stringify(env[settings])));
  console.log(chalk.blue('-----------------------------------'));
  return env[settings];
}

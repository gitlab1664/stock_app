const express = require("express")

const router = express.Router()

router.get('/api/echo', function (req, resp) {
  console.log('echo route invoked')
  resp.send('echo')
})

module.exports = router

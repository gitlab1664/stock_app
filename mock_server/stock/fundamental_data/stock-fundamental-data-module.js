const express = require('express');
const router = express.Router();
const client = require('./stock-fundamental-data-api-client');
const chalk = require("chalk");


router.get('/api/stock/fundamental/:symbol', (req, res) => {
    console.log(chalk.green(`Symbol to get company overview: ${req.params.symbol}`));
    const symbol = req.params.symbol;

    client.getCompanyOverview(symbol, result => {
        if (!result) {
            console.log(chalk.red('Failed to fetch data'));
            res.status(404).json({
                success: false,
                msg: `Failed to get data for ${symbol}`
            });
        }
        res.json(result.data);
    });
});

module.exports = router
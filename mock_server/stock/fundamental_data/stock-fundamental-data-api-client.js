const axios = require('axios');
const chalk = require('chalk');
const configs = require('../../shared/configs');
const conf = configs.config();

const baseUrl = conf.financialApiUrl;
const apiKey = conf.financialApiKey;

const getCompanyOverview = (symbol, callback) => {
    console.log(chalk.green(`Try to get company info for: ${symbol} from remote service`));
    axios.get(baseUrl.concat(`/query?function=OVERVIEW&symbol=${symbol}&apikey=${apiKey}`))
        .then(response => {
            callback(response);
        })
        .catch(error => {
            console.log(chalk.red('Failed to get company overview'));
            console.error(chalk.red(error));
            callback(undefined);
        });
}

module.exports = {
    getCompanyOverview
}
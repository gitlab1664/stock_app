const express = require('express');
const router = express.Router();
const client = require('./stock-time-series-api-client');
const chalk = require("chalk");

router.get('/api/stock/:symbol', (req, res) => {
    console.log(chalk.green(`Symbol to find: ${req.params.symbol}`));
    const symbol = req.params.symbol;

    client.getDailyBySymbol(symbol, (result) => {
        if (!result) {
            console.log(chalk.red(`Failed to fetch data for symbol: ${symbol}`));
            res.status(404).json({
                success: false,
                msg: `Failed to get data for ${symbol}`
            });
        }
        res.json({
            symbol: symbol,
            values: Object.values(result.data['Time Series (Daily)'])
        });
    });
});

module.exports = router;

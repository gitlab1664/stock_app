const axios = require('axios');
const chalk = require("chalk");
const configs = require('../../shared/configs');
const conf = configs.config();


const baseUrl = conf.financialApiUrl;
const apiKey = conf.financialApiKey;

const getDailyBySymbol = (symbol, callback) => {
    console.log(chalk.green(`Try to get stock data for symbol: ${symbol} from remote service`))
    axios.get(baseUrl.concat(`/query?function=TIME_SERIES_DAILY&symbol=${symbol}&apikey=${apiKey}`))
        .then(response => {
            callback(response);
        })
        .catch(error => {
            console.error(chalk.red(error));
            callback(undefined);
        });
}

module.exports = {
    getDailyBySymbol
}

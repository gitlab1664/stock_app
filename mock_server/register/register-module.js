const express = require('express')
const RegisterModel = require("./register-schema")
const UserModel = require("./user-schema")
const router = express.Router()

router.post("/api/register", (req, res) => {

  const registered = registerUser(req.body);
  registered.then(registered => {
      saveUser(registered).then(() => {
        res.json({
          result: true,
          errors: []
        })
      }).catch(() => {
        res.status(400).json({
          msg: 'failed to save user login and password'
        })
      })
  }).catch(() => {
    res.status(400).json({
      msg: 'failed to save new user data'
    })
  })
})

router.get('/api/register/questions', (req, res) => {
  res.json(mockQuestions)
})

module.exports = router

async function registerUser(newUser) {
  let model = new RegisterModel(newUser);
  return await model.save().then(saved => {
    console.log(`register data: ${saved}`);
    return saved;
  }).catch(reason => {
    console.log(`mongo register user failed ${reason}`);
    throw reason;
  });
}

async function saveUser(userData) {
  const {email, password} = userData;
  const newUser = {email, password};
  let userModel = new UserModel(newUser)

  return await userModel.save().then(saved => {
    console.log(`user saved ${saved}`);
  }).catch(reason => {
    console.log(`failed to save user ${reason}`);
    throw reason;
  })
}

const mockQuestions =
  [
    {
      key: 'investor',
      label: 'Hava you ever been investing on stock market?',
      options: [
        {key: 'yes', value: 'Yes'},
        {key: 'no', value: 'No'},
        {key: 'duno', value: 'I dont know'}
      ],
      order: 3,
      controlType: 'dropdown'
    },

    {
      key: 'nick',
      label: 'If you want u can set nickname',
      value: '',
      required: false,
      order: 1,
      controlType: 'textbox'
    },

    {
      key: 'info',
      label: 'You can write something about yourself for profile',
      order: 2,
      controlType: 'textbox'
    }
  ]

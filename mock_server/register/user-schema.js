const uuid = require('uuid');
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  externalId: {
    type: String,
    default: uuid.v1().toString()
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
})

module.exports = mongoose.model('User', UserSchema, 'user_data')

const uuid = require('uuid');
const mongoose = require('mongoose')

const RegisterSchema = new mongoose.Schema({
  externalId: {
    type: String,
    default: uuid.v1().toString()
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  birthData: {
    type: Date,
    required: true
  },
  conditionsAgreement: Boolean,
  questions: []
});

module.exports = mongoose.model('Register', RegisterSchema, 'register_data')

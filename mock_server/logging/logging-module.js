const express = require('express')
const UserModel = require("../register/user-schema")
const router = express.Router()

router.post("/api/login", (req, res) => {
  console.log(`attempt to login: ${req.body.email} with: ${req.body.password}`)

  if (!req || !req.body) {
    res.status(400).json({
      msg: 'invalid request'
    });
  }

  UserModel.findOne({'email': `${req.body.email}`}, (err, result) => {
    if (err) {
      console.log(`failed to get user for login ${req.body}`);
      res.status(400).json({
        msg: 'failed to fetch user data'
      });
    } else {
      if (!result && !err) {
        res.status(400).json({
          msg: 'failed to find user with given login'
        });
      } else if (result.email === req.body.email && result.password === req.body.password) {
        console.log('User logged');
        res.json({
          result: true,
          errors: []
        });
      } else {
        console.log(`invalid login and password combination ${req.body}`);
        res.status(400).json({
          msg: 'invalid login password combination'
        });
      }
    }
  });
})

module.exports = router;

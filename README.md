Kubernates (bare metal):
-
Used architecture for bare metal centos 7:

- metallb - load balancer for bare metal linux (metallbconfig.yaml)
- nginx ingress controller - because I have load balancer ingress controller has changed type from NodePort to
  LoadBalancer
  (ingress-controller.yml).
- deployments-* files have all configs to start application

Minikube:
- 

- use this file to start application from minikube deployment-minikube.yml

Notes:
-

- remember to check/add entries to /etc/hosts to use dns names instead of ip addresses
- **without ingres but with load balancer, to expose service/application simply change service type from ClusterIp to
  LoadBalancer. Pod should get external ip and should be accessible**.
- !! **in case communication suddenly fails between pods  just restart dns** !!  
  kubectl -n kube-system rollout restart deployment coredns

Stock api key
-
https://www.alphavantage.co/
4ZVU73QJCOU09W5V

TODO:
-
- google cloud kubernates